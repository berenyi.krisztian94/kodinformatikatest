package com.example.KodInformatikaTest.repository;

import com.example.KodInformatikaTest.model.ExchangeValutaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeValutaRepository extends JpaRepository<ExchangeValutaEntity, Long> {
}
