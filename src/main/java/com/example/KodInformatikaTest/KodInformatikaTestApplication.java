package com.example.KodInformatikaTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KodInformatikaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(KodInformatikaTestApplication.class, args);
	}

}
