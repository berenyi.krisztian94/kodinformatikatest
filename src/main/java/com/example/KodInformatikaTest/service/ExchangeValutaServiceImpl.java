package com.example.KodInformatikaTest.service;

import com.example.KodInformatikaTest.repository.ExchangeValutaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExchangeValutaServiceImpl implements ExchangeValutaService{

    @Autowired
    private final ExchangeValutaRepository exchangeValutaRepository;

    public ExchangeValutaServiceImpl(ExchangeValutaRepository exchangeValutaRepository) {
        this.exchangeValutaRepository = exchangeValutaRepository;
    }

    public List<String> findAllValuta(){
        return exchangeValutaRepository.findAll().stream().map(ev->ev.getValuta()).collect(Collectors.toList());
    }
}
