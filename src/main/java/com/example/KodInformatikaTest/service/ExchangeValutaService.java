package com.example.KodInformatikaTest.service;


import java.util.List;


public interface ExchangeValutaService {
    List<String> findAllValuta();
}
