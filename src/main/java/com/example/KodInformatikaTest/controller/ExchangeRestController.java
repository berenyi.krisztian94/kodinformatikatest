package com.example.KodInformatikaTest.controller;

import com.example.KodInformatikaTest.service.ExchangeValutaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExchangeRestController {

    enum Valuta{
        EUR,
        HUF
    }

    private final ExchangeValutaService exchangeValutaService;

    @Autowired
    public ExchangeRestController(ExchangeValutaService exchangeValutaService) {
        this.exchangeValutaService = exchangeValutaService;
    }

    @GetMapping("/symbols")
    public List<String> allSymbols(){
        return exchangeValutaService.findAllValuta();
    }

    @GetMapping("/convert")
    public int convert(@RequestParam("from") String from,
                       @RequestParam("to") String to,
                       @RequestParam("amount") int amount) {
        return from.equals(Valuta.EUR) ? (int)amount/330 : (int)amount*330;
    }

}
