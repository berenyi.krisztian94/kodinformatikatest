create sequence exchange_valuta_entity_seq;

CREATE TABLE exchange_valuta_entity
(
    id     bigint not null default nextval('exchange_valuta_entity_seq'),
    valuta varchar(4)
);


INSERT INTO exchange_valuta_entity(valuta) values('HUF');
INSERT INTO exchange_valuta_entity(valuta) values('EUR');

ALTER SEQUENCE exchange_valuta_entity_seq
    OWNED BY exchange_valuta_entity.id;