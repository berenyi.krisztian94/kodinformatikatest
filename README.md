dcoker yml file:
version: '3.6'

services:
  test-database:
    image: "postgres"
    restart: always
    ports:
      - "1008:5432"
    environment:
      POSTGRES_PASSWORD: "password"
      POSTGRES_USER: "admin"
    network_mode: bridge
